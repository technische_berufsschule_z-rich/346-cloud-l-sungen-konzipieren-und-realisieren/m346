# Host-Rechner

Der Host-Rechner hat 15.7 GB RAM verfügbar und 8 CPUs.
dies kann man sehr leicht im Taskmanager nachschauen
 

![Host info](ProzessorenRam.png)


 

# VM Virtualbox
ich habe als VM Virtualbox ausgewählt und habe die Windows Variante runtergeladen und installiert.
![VM Settings](vmdownload.png)

 

## Weniger  als Host

Verwendet: 1 CPUs, 1 GB RAM
Die Zuteilung hat in den Einstellungen stattgefunden das geht nur wenn man die vm auschaltet.

hier stelle ich cpu ein auf 1
![cpu Einstellung](1CPU.png)
hier stelle ich Ram auf 1 GB
![Ram Einstellung](1GBRam.png)
hier teste ich die CPU
![cpu weniger](wenigercpus.png)
hier teste ich RAM
![ram weniger](wenigerRam.png)

 

## Mehr als Host

Verwendet: 8 CPUs, 16 GB RAM
Hier wird mir eine Meldung gegeben das ich die VM gar nicht starten kann weil ich nicht so viel cpu und Ram zur Verfügung habe 

![maximal](maximalProzessorundRam.png)


 

<br>

 Eine Fehlermeldung tritt auf, wenn die zugewiesenen Ressourcen einer virtuellen Maschine (VM) die Kapazität des Host-Systems überschreiten. Dies geschieht, da das Host-Betriebssystem die Ressourcen zwischen den VMs und dem Host selbst verteilen muss. Wenn eine VM mehr CPU und RAM beansprucht als der Host bereitstellen kann, führt dies zu Ressourcenengpässen und einer Beeinträchtigung der Gesamtleistung, was in einer Fehlermeldung resultiert.

 

